package com.akademiakodu.phonebook;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiFactory {
    public static Api getApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://recruitment-api.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(Api.class);
    }
}

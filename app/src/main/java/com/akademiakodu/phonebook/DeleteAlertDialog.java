package com.akademiakodu.phonebook;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import retrofit2.Call;
import retrofit2.Callback;

public class DeleteAlertDialog {
    public static void showDialog(Context context,
                                  final Call<Void> callOnAccept,
                                  final Callback<Void> callback) {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("Usuń")
                .setMessage("Czy na pewno chcesz usunąć ten kontakt ?")
                .setNegativeButton("Nie", (dialogInterface, __) -> dialogInterface.cancel())
                .setPositiveButton("Tak", (dialog1, __) -> {
                    dialog1.cancel();
                    callOnAccept.enqueue(callback);
                }).create();
        dialog.show();
    }
}

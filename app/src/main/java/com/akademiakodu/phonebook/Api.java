package com.akademiakodu.phonebook;

import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface Api {
    @GET("phonebook/")
    Call<List<Contact>> listContacts();

    @PUT("phonebook/")
    Call<Contact> addContact(@Body Contact entry);

    @POST("phonebook/{id}")
    Call<Contact> editContact(@Path("id") UUID id, @Body Contact entry);

    @DELETE("phonebook/{id}")
    Call<Void> deleteContact(@Path("id") UUID id);
}

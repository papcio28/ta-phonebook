package com.akademiakodu.phonebook;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {
    private List<Contact> mContacts = Collections.emptyList();
    private ItemActionsListener mActionsListener;

    public ContactAdapter(ItemActionsListener actionsListener) {
        mActionsListener = actionsListener;
    }

    public void setContacts(List<Contact> contacts) {
        mContacts = contacts;
        notifyDataSetChanged();
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ContactViewHolder(inflater.inflate(R.layout.layout_contact_row, parent, false));
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        Contact contact = mContacts.get(position);

        holder.name.setText(contact.getName());
        holder.number.setText(contact.getNumber());
        holder.email.setText(contact.getEmail());
        holder.currentContact = contact;
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        protected TextView name;

        @BindView(R.id.phone)
        protected TextView number;

        @BindView(R.id.email)
        protected TextView email;

        private Contact currentContact;

        public ContactViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.call)
        protected void onCallClick() {
            if (mActionsListener != null) {
                mActionsListener.onCallClick(currentContact);
            }
        }

        @OnClick(R.id.delete)
        protected void onDeleteClick() {
            if (mActionsListener != null) {
                mActionsListener.onDeleteClick(currentContact);
            }
        }

        @OnClick
        protected void onEditClick() {
            if (mActionsListener != null) {
                mActionsListener.onEditClick(currentContact);
            }
        }
    }

    public interface ItemActionsListener {
        void onCallClick(Contact contact);

        void onDeleteClick(Contact contact);

        void onEditClick(Contact contact);
    }
}

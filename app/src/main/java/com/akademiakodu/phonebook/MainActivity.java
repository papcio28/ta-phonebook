package com.akademiakodu.phonebook;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Callback<List<Contact>>,
        ContactAdapter.ItemActionsListener {
    private Api mApi;

    @BindView(R.id.contact_list)
    protected RecyclerView mList;

    private ContactAdapter mAdapter;
    private Contact mPendingContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mPendingContact = (Contact) getLastCustomNonConfigurationInstance();

        mAdapter = new ContactAdapter(this);
        mList.setLayoutManager(new LinearLayoutManager(this));
        mList.setAdapter(mAdapter);

        mApi = ApiFactory.getApi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadListData();
    }

    private void reloadListData() {
        mApi.listContacts().enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
        if (response.isSuccessful()) {
            mAdapter.setContacts(Stream.of(response.body())
                    .sorted(this::compareContacts)
                    .collect(Collectors.toList()));
        }
    }

    private int compareContacts(Contact o1, Contact o2) {
        return o1.getName().compareToIgnoreCase(o2.getName());
    }

    @Override
    public void onFailure(Call<List<Contact>> call, Throwable t) {
        System.out.println(t);
    }

    @Override
    public void onCallClick(Contact contact) {
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contact.getNumber()));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            mPendingContact = contact;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 10);
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onDeleteClick(Contact contact) {
        DeleteAlertDialog.showDialog(this, mApi.deleteContact(contact.getId()), new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                reloadListData();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    @Override
    public void onEditClick(Contact contact) {
        EditActivity.start(this, contact);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 10) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Uprawnienie zostało zaakceptowane przez użytkownika
                onCallClick(mPendingContact);
                mPendingContact = null;
            } else {
                Toast.makeText(this, "Zaakceptuj uprawnienia, żeby dzwonić !", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return mPendingContact;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_add) {
            startActivity(new Intent(this, EntryActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

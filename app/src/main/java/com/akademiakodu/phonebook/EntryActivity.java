package com.akademiakodu.phonebook;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EntryActivity extends AppCompatActivity implements Callback<Contact> {
    @BindView(R.id.name)
    protected EditText mName;

    @BindView(R.id.phone)
    protected EditText mNumber;

    @BindView(R.id.email)
    protected EditText mEmail;

    @BindView(R.id.save)
    protected Button mSaveButton;

    protected Api mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);
        ButterKnife.bind(this);

        mApi = ApiFactory.getApi();
    }

    @OnClick(R.id.save)
    protected void onSaveClick() {
        String nameValue = mName.getText().toString().trim();
        String phoneValue = mNumber.getText().toString().trim();
        String emailValue = mEmail.getText().toString().trim();

        if (nameValue.length() > 4 && (phoneValue.length() > 4 || emailValue.length() > 4)) {
            Contact contact = new Contact();
            contact.setName(nameValue);
            contact.setNumber(phoneValue);
            contact.setEmail(emailValue);

            getSaveCall(contact).enqueue(this);
        } else {
            Toast.makeText(this, "Błąd w formularzu !", Toast.LENGTH_SHORT).show();
        }
    }

    protected Call<Contact> getSaveCall(Contact contact) {
        return mApi.addContact(contact);
    }

    @Override
    public void onResponse(Call<Contact> call, Response<Contact> response) {
        if (response.isSuccessful()) {
            finish();
            return;
        }
        try {
            Toast.makeText(this, response.errorBody().string(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Call<Contact> call, Throwable t) {

    }
}

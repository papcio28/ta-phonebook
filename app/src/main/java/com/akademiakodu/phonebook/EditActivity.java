package com.akademiakodu.phonebook;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import retrofit2.Call;

public class EditActivity extends EntryActivity {
    public static final String EXTRA_CONTACT = "contact";

    public static void start(Context context, Contact contact) {
        Intent startIntent = new Intent(context, EditActivity.class);
        startIntent.putExtra(EXTRA_CONTACT, contact);

        context.startActivity(startIntent);
    }

    private Contact mContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Ustawiamy początkowe dane na formularzu
        mContact = (Contact) getIntent().getSerializableExtra(EXTRA_CONTACT);
        mName.setText(mContact.getName());
        mNumber.setText(mContact.getNumber());
        mEmail.setText(mContact.getEmail());
    }

    @Override
    protected Call<Contact> getSaveCall(Contact contact) {
        return mApi.editContact(mContact.getId(), contact);
    }
}
